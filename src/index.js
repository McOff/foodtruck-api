import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;

import config from './config';
import routes from './routes';

let app = express();
app.server = http.createServer(app);

//middleware
app.use(bodyParser.json({
  limit: config.bodyLimit
}))
// passport config

app.use(passport.initialize());
const Account = require('./model/account_model');

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, Account.authenticate());

passport.use(localLogin);
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());


// api routes v1

app.use('/v1', routes);

app.use((err, req, res, next ) => {
  res.status(422).send({error: err.message});
});

app.server.listen(config.port);
console.log(`Started on port ${app.server.address().port}`);

export default app;
