import mongoose from 'mongoose';
import FoodTruck from './foodtruck_model';
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  text: String,
  foodtruck: {
    type: Schema.Types.ObjectId,
    ref: 'FoodTruck',
    require: true
  }
});

module.exports = mongoose.model('Review', ReviewSchema);
