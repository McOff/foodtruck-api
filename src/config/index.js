export default {
  "port": 3005,
  "bodyLimit": "100kb",
  "mongoUrl": "mongodb://localhost:27017/foodtruck-api"
}
