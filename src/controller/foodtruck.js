import mongoose from 'mongoose';
import { Router } from 'express';
import FoodTruck from '../model/foodtruck_model';
import Review from '../model/review_model';

import { authenticate } from '../middleware/authmiddleware';

export default ({ config, db }) => {
  let api = Router();

  // '/v1/foodtruck/add'
  api.post('/add', authenticate,  (req, res) => {
    const { name, foodtype, avgcost, geometry } = req.body;
    let newTruck = new FoodTruck({
      name, foodtype, avgcost, geometry
    });

    newTruck.save(err => {
      if (err) {
        return res.json(err);
      }
      res.json({ message: 'FoodTruck saved successfully'});
    });
  })

  // '/v1/foodtruck' - Read

  api.get('/', (req, res) => {
    FoodTruck.find({}, (err, foodtrucks) => {
      if (err) {
        return res.json(err);
      }

      res.json(foodtrucks);
    });
  });

  // '/v1/foodtruck/:id' - Read 1
  api.get('/:id', (req, res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if (err) {
        return res.json(err);
      }
      res.json(foodtruck);
    });
  });

  // 'v1/foodtruck/:id' - Update

  api.put('/:id', (req, res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if (err) {
        return res.json(err);
      }
      foodtruck.name = req.body.name;
      foodtruck.save(err => {
        if (err) {
          return res.json(err);
        }
        res.json({ message: "FoodTruck info updated"});
      });
    });
  });

  // '/v1/foodtruck/:id' - Delete

  api.delete('/:id', (req, res) => {
    FoodTruck.remove({
      _id: req.params.id
    }, (err, foodtruck) => {
      if (err) {
        return res.json(err);
      }
      res.json({ message: "FoodTruck successfully remooved!" });
    })
  });

  // add review for a specific foodtruck id
  // '/v1/foodtruck/reviews/add/:id'
  api.post('/reviews/add/:id', (req, res) => {
    FoodTruck.findById(req.params.id, (err, foodtruck) => {
      if (err) {
        return res.json(err);
      }
      const { title, text } = req.body;
      const newReview = new Review({
        title, text,
        foodtruck: foodtruck._id
      });

      newReview.save( (err, review) => {
        if (err) {
          return res.json(err);
        }
        foodtruck.reviews.push(newReview);
        foodtruck.save(err => {
          if (err) {
            return res.json(err);
          }
          res.json({ message: 'Food truck review saved!'});
        });
      });
    });
  });

  //get reviews for a specific food truck id
  // 'v1/foodtruck/review/:id'

  api.get('/reviews/:id', (req, res) => {
    Review.find({ foodtruck: req.params.id }, (err, reviews) => {
      if (err) {
        return res.json(err);
      }
      res.json(reviews);
    })
  })

  return api;
}
